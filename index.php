<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Card NFC</title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="assets/css/form-elements.css">
        <link rel="stylesheet" href="assets/css/style.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="assets/ico/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">

    </head>

   

    <body  >
<script type="text/javascript">
function disableF5(e) { if ((e.which || e.keyCode) == 116 || (e.which || e.keyCode) == 82) e.preventDefault(); };

$(document).ready(function(){
     $(document).on("keydown", disableF5);
});
</script>

        <!-- Top content -->
        <div class="top-content">
        	
            <div class="inner-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2 text">
                            <h1><strong>NFC</strong> Reader</h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 form-box">
                        	<div class="form-top">
                        		<div class="form-top-left">
                        			<h3></h3>
                            		<p></p>
                        		</div>
                        		<div class="form-top-right">
                        			  
 <img src="assets/nfc.png" alt="Person" class="card__image">
                        		</div>
                            </div>
                            <div class="form-bottom">
			                    <form role="form" action="" method="post" class="login-form" id="myform" name="myform">
			                        <div class="form-group" id="result">
			                        	<label class="sr-only" for="form-username">Card Number</label>
                                        <input type="text" name="form-username" autofocus="autofocus" placeholder="Card Number..." class="form-username form-control" id="text" onchange="myFunction(this.value)">
			                        </div>
			                    <!--     <button type="submit" class="btn">Continue</button> -->
			                    </form>
		                    </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>

<div class="footer-copyright">
    <div class="container">
    @2021 Developed By
    <a class="grey-text text-lighten-4 right" href="https://www.vintall.com/" target="_blank">Vintall</a>
        <!-- Javascript -->
        <script src="assets/js/jquery-1.11.1.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/js/jquery.backstretch.min.js"></script>
        <script src="assets/js/scripts.js"></script>
    </div></div>
        
        <!--[if lt IE 10]>
            <script src="assets/js/placeholder.js"></script>
        <![endif]-->

        <script>
function myFunction(val) {
 // alert("The input value has changed. The new value is: " + val);
 window.open('sendata.php?datasss=' + val); 
  // $.ajax({
  //       type: "GET",/*method type*/
  //       contentType: "application/json",
  //       url: "./sendata.php/?datasss="+val,/*Target function that will be return result*/
  //       // data: {"datasss":datt},//parameter pass data is parameter name param is value 
  //       dataType: "json",
  //       success: function(data) {
  //              alert(data);
  //       },
  //       error: function(result) {
  //           alert("Error");
  //       }
  //   });
}
</script>


    </body>

</html>